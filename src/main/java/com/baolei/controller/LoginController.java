package com.baolei.controller;

import com.baolei.pojo.Emp;
import com.baolei.pojo.Result;
import com.baolei.service.EmpService;
import com.baolei.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@Slf4j
@RestController
public class LoginController {

    @Autowired
    EmpService empService;

    @PostMapping("/login")
    public Result login(@RequestBody Emp emp){
        log.info("用户名：{}，密码：{}",emp.getUsername(), emp.getPassword());
        Emp loginEmp = empService.login(emp);
        if (loginEmp != null){
            Map<String ,Object> claims = new HashMap<>();
            claims.put("id",loginEmp.getId());
            claims.put("username",loginEmp.getUsername());
            claims.put("name",loginEmp.getName());

            String token = JwtUtils.generateJwt(claims);
            return Result.success(token);
        }

        return Result.error("用户名或密码错误");
    }
}
