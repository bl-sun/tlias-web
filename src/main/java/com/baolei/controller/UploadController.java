package com.baolei.controller;

import com.baolei.pojo.Result;
import com.baolei.util.AliOSSUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    AliOSSUtils aliOSSUtils;
    @PostMapping
    public Result upload(MultipartFile image) throws IOException {
        log.info("文件上传：{}, {}, {}", image, image.getOriginalFilename(), image.getSize());
        String url = aliOSSUtils.upload(image);
        log.info("文件访问的url： {}", url);
        return Result.success(url);
    }
}
