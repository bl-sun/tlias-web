package com.baolei.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.baolei.pojo.Result;
import com.baolei.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class LoginCheckInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 1.获取请求地址
        String url = request.getRequestURL().toString();
        log.info("请求的url: {}",url);

        if (url.contains("login")){
            log.info("执行登录操作，放行");
            return true;
        }

        // 2.获取请求头token 判断token
        String token = request.getHeader("token");

        // 3. 判断token

        if (!StringUtils.hasLength(token)){
            log.info("没有token重新登陆");
            Result result = Result.error("NOT_LOGIN");
            String jsonString = JSONObject.toJSONString(result);
            response.getWriter().write(jsonString); //不放行直接返回
            return false;
        }

        // 4. 解析token
        try {
            JwtUtils.parseJWT(token);
        }catch (Exception e){
            log.info("令牌解析失败");
            Result result = Result.error("NOT_LOGIN");
            String jsonString = JSONObject.toJSONString(result);
            response.getWriter().write(jsonString);
            return false;
        }

        //5. 放行
        return true;
    }
}
