package com.baolei.service.impl;

import com.baolei.anno.Log;
import com.baolei.mapper.EmpMapper;
import com.baolei.pojo.Emp;
import com.baolei.pojo.PageBean;
import com.baolei.service.EmpService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpMapper empMapper;
    @Override
    public PageBean<Emp> page(Integer page, Integer pageSize, String name, Short gender, LocalDate begin, LocalDate end) {
        //1. 设置分页参数
        PageHelper.startPage(page,pageSize);
        //2. 封装为page对象
        Page<Emp> p = (Page<Emp>)empMapper.list(name,gender,begin,end);
        //3. 封装为page bean对象返给前端
        return new PageBean(p.getTotal(),p.getResult());
    }

    @Override
    @Log
    public void delete(List<Integer> ids) {
        empMapper.delete(ids);
    }

    @Override
    @Log
    public void save(Emp emp) {
        emp.setCreateTime(LocalDateTime.now());
        emp.setUpdateTime(LocalDateTime.now());
        empMapper.insert(emp);
    }

    @Override
    public Emp getById(Integer id) {
        Emp emp = empMapper.getById(id);
        return emp;
    }

    @Override
    public void update(Emp emp) {
        emp.setUpdateTime(LocalDateTime.now());
        empMapper.update(emp);
    }

    @Override
    @Log
    public Emp login(Emp emp) {
        return empMapper.getByUsernameAndPassword(emp);
    }
}
