package com.baolei.service.impl;

import com.baolei.mapper.DeptLogMapper;
import com.baolei.pojo.DeptLog;
import com.baolei.service.DeptLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DeptLogServiceImpl implements DeptLogService {
    @Autowired
    DeptLogMapper deptLogMapper;
    @Override
    @Transactional(propagation = Propagation.REQUIRED) //默认为REQUIRED 有事务则加入没事务则新建事务
    public void insert(DeptLog deptLog) {
        deptLogMapper.insert(deptLog);
    }
}
