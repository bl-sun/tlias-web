package com.baolei.service.impl;

import com.baolei.anno.Log;
import com.baolei.mapper.DeptMapper;
import com.baolei.mapper.EmpMapper;
import com.baolei.pojo.Dept;
import com.baolei.pojo.DeptLog;
import com.baolei.service.DeptLogService;
import com.baolei.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptMapper deptMapper;
    @Autowired
    private EmpMapper empMapper;

    @Autowired
    private DeptLogService deptLogService;
    @Override
    public List<Dept> list() {
        return deptMapper.list();
    }

    /**
     * 根据部门id删除部门
     * 并删除部门下的所有员工
     * @param id
     */
    @Override
    @Log
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        try{
            deptMapper.deleteById(id);
            empMapper.deleteByDeptId(id);
        }finally {
            DeptLog deptLog = new DeptLog();
            deptLog.setCreateTime(LocalDateTime.now());
            deptLog.setDescription("执行了解散部门的操作，此时解散的是"+id+"号部门");
            deptLogService.insert(deptLog);
        }

    }

    @Override
    @Log
    public void add(Dept dept) {
        dept.setCreateTime(LocalDateTime.now());
        dept.setUpdateTime(LocalDateTime.now());
        deptMapper.insert(dept);
    }


}
