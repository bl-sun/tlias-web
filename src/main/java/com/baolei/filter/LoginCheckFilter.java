package com.baolei.filter;

import com.alibaba.fastjson.JSONObject;
import com.baolei.pojo.Result;
import com.baolei.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Slf4j
//@WebFilter(urlPatterns = "/*")
public class LoginCheckFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;

        // 1.获取请求地址
        String url = req.getRequestURL().toString();
        log.info("请求的url: {}",url);

        if (url.contains("login")){
            log.info("执行登录操作，放行");
            filterChain.doFilter(servletRequest,servletResponse);
            return;
        }
        
        // 2.获取请求头token 判断token
        String token = req.getHeader("token");

        // 3. 判断token

        if (!StringUtils.hasLength(token)){
            log.info("没有token重新登陆");
            Result result = Result.error("NOT_LOGIN");
            String jsonString = JSONObject.toJSONString(result);
            servletResponse.getWriter().write(jsonString); //不放行直接返回
            return;
        }

        // 4. 解析token
        try {
            JwtUtils.parseJWT(token);
        }catch (Exception e){
            log.info("令牌解析失败");
            Result result = Result.error("NOT_LOGIN");
            String jsonString = JSONObject.toJSONString(result);
            servletResponse.getWriter().write(jsonString);
            return;
        }

        //5. 放行
        filterChain.doFilter(servletRequest,servletResponse);

    }
}
