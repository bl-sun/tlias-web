package com.baolei.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
@Slf4j
@WebFilter(urlPatterns = "/*")
public class DemoFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("filter初始化");
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("放行前的逻辑");
        filterChain.doFilter(servletRequest,servletResponse);
        log.info("放行后的逻辑");
    }

    @Override
    public void destroy() {
        log.info("filter销毁");
        Filter.super.destroy();
    }
}
