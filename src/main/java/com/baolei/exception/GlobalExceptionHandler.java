package com.baolei.exception;

import com.baolei.pojo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Result ex(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        return Result.error("对不起，操作失败，请联系管理员");
    }
}
