package com.baolei;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class TliasWebApplicationTests {

	@Test
	void contextLoads() {
	}

	/**
	 * 测试jwt令牌的生成
	 */
	@Test
	void genJWT(){
		Map<String, Object> claims = new HashMap();
		claims.put("id",1);
		claims.put("name","zhs");


		JwtBuilder jwtBuilder = Jwts.builder().signWith(SignatureAlgorithm.HS256, "baolei").setClaims(claims).setExpiration(new Date(System.currentTimeMillis() + 3600 * 1000));

		String compact = jwtBuilder.compact();
		System.out.println(compact); // eyJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiemhzIiwiaWQiOjEsImV4cCI6MTY5MjMzMTY0Mn0.GRfbajDzToK1gacfS5KIdZ0eb9nmTz3uIbEXSUGgVHs

	}

	@Test
	void parseJwt(){
//		Claims claims = Jwts.parser()
//				.setSigningKey("baolei")
//				.parseClaimsJws("eyJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiemhzIiwiaWQiOjEsImV4cCI6MTY5MjMzMTY0Mn0.GRfbajDzToK1gacfS5KIdZ0eb9nmTz3uIbEXSUGgVHs")
//				.getBody();
//		System.out.println(claims);
	}

}
