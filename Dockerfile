FROM openjdk:17.0.2-jdk
EXPOSE 8080:8080
COPY ./target/*.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]